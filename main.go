package main

import (
	"crawler_translation/data"
	"crawler_translation/reptile"
	"crawler_translation/utils"
	"fmt"
)

func main() {
	if err := utils.InitLogger(); err != nil {
		fmt.Println(fmt.Sprintf("日志模块初始化失败 err = %s", err))
		return
	}
	if err := data.NewMysql(); err != nil {
		fmt.Println(fmt.Sprintf("mysql模块初始化失败 err = %s", err))
		return
	}

	// 读取数据
	urls, err := data.MConn.GetMenuList()
	if err != nil {
		fmt.Println("读取menu失败 err = ", err)
		return
	}

	utils.Logger.Info("爬取开始")

	aptosReptile := reptile.NewReptile(urls, 5, ".theme-doc-markdown")
	err = aptosReptile.Run()
	if err != nil {
		utils.Logger.Error(fmt.Sprintf("启动爬虫失败: %s", err))
		return
	}

	utils.Logger.Info("爬取结束")
}
