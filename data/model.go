package data

type Menu struct {
	Id       int    `db:"id"`
	Title    string `db:"title"`
	TitleZh  string `db:"title_zh"`
	Domain   string `db:"domain"`
	Path     string `db:"path"`
	ParentId int    `db:"parent_id"`
	Hash     string `db:"hash"`
	IsClick  int    `db:"is_click"`
}

type Docs struct {
	Id          int    `db:"id"`
	Title       string `json:"title"`
	Keyword     string `json:"keyword"`
	Description string `json:"description"`
	Content     string `json:"content"`
	Path        string `json:"path"`
}
