## 英文网页翻译为中文
 
爬虫模块`reptile` ，使用jQuery用法获取指定dom。翻译模块`translate` ，将html文本翻译为中文html文本

### 示例
- 英文网页
```html
<h2 class="anchor anchorWithStickyNavbar_LWe7" id="clone-the-aptos-core-repo">Clone the Aptos-core repo<a class="hash-link" href="#clone-the-aptos-core-repo" title="Direct link to heading">&ZeroWidthSpace;</a></h2><p>As described in <a href="#workflows">Workflows</a>, you may interact with Aptos using only the CLI. If you need the source, clone the <code>aptos-core</code> GitHub repo from <a href="https://github.com/aptos-labs/aptos-core" target="_blank" rel="noopener noreferrer">GitHub</a>.</p>
```
- 中文（翻译后）
```html
<h2 class="anchor anchorWithStickyNavbar_LWe7" id="clone-the-aptos-core-repo">克隆 Aptos-core 存储库<a class="hash-link" href="#clone-the-aptos-core-repo" title="Direct link to heading"></a></h2><p>如中所述<a href="#workflows">工作流程</a>，您可以仅使用 CLI 与 Aptos 交互，如果您需要源代码，请克隆<code>aptos-core</code>来自的 GitHub 存储库<a href="https://github,com/aptos-labs/aptos-core" target="_blank" rel="noopener noreferrer">GitHub</a>,</p>
```