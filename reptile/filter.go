package reptile

import (
	"regexp"
)

/**
过滤不翻译的标签
*/

type Filter struct {
}

func (f *Filter) getHtml(html string) (string, []string) {
	expr := `<code([\s\S]*?)>([\s\S]*?)</code>`
	reg := regexp.MustCompile(expr)

	var posMap []string
	for _, val := range reg.FindAllString(html, -1) {
		posMap = append(posMap, val)
	}

	newHtml := reg.ReplaceAllStringFunc(html, func(s string) string {
		return "[占位]"
	})

	return newHtml, posMap
}
