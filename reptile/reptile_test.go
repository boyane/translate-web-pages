package reptile

import (
	"crawler_translation/data"
	"crawler_translation/translate"
	"crawler_translation/utils"
	"fmt"
	"testing"
)

func TestReptileWeb(t *testing.T) {
	//var menus []data.Menu
	//menus = append(menus, data.Menu{
	//	Domain: "https://aptos.dev",
	//	Path:   "/whats-new-in-docs",
	//})
	//
	//reptile := NewReptile(menus, 10)
	//
	//// 爬取网页内容
	//pageBody, err := reptile.crawlingWebPage(menus[0].Domain + menus[0].Path)
	//if err != nil {
	//	fmt.Println(err)
	//	return
	//}
	//
	//// 获取指定html中的内容
	//content, err := reptile.getBodyByClass(pageBody, func(doc *goquery.Document) (data string, err error) {
	//	data, err = doc.Find(".theme-doc-markdown").Html()
	//	if err != nil {
	//		return "", fmt.Errorf("获取getBodyClass [.theme-doc-markdown] err = %s", err)
	//	}
	//	return
	//})
	//if err != nil {
	//	fmt.Println(err)
	//	return
	//}

	content := "a​b"

	specialMap := make(map[int]struct{})
	specialMap[8203] = struct{}{}
	newString := utils.RmString(content, specialMap)

	fmt.Println(newString)
}

// 翻译标题
func TestFilterCode(t *testing.T) {
	if err := utils.InitLogger(); err != nil {
		fmt.Println(fmt.Sprintf("日志模块初始化失败 err = %s", err))
		return
	}
	if err := data.NewMysql(); err != nil {
		fmt.Println(fmt.Sprintf("mysql模块初始化失败 err = %s", err))
		return
	}

	// 获取标题
	paths, err := data.MConn.GetMenuList()
	if err != nil {
		fmt.Println("读取menu失败 err = ", err)
		return
	}

	translate2zh := translate.NewTranslate()
	// 翻译修改
	for _, val := range paths {
		zh, err := translate2zh.TranslateEn2Ch(val.Title)
		if err != nil {
			fmt.Println(err)
			return
		}

		_, err = data.MConn.Conn.Exec("UPDATE menu SET title_zh=? WHERE id=?", zh, val.Id)
		if err != nil {
			fmt.Println(err)
			return
		}
	}
}
