module crawler_translation

go 1.19

require (
	github.com/PuerkitoBio/goquery v1.8.0
	github.com/go-sql-driver/mysql v1.6.0
	github.com/jmoiron/sqlx v1.3.5
	go.uber.org/zap v1.23.0
)

require (
	github.com/andybalholm/cascadia v1.3.1 // indirect
	go.uber.org/atomic v1.10.0 // indirect
	go.uber.org/multierr v1.8.0 // indirect
	golang.org/x/net v0.1.0 // indirect
	golang.org/x/text v0.4.0 // indirect
)
